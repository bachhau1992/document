<?php 
ob_start();
session_start();
	// Autoload
function __autoload($className)
{
	if(strpos($className, 'Controller') !== false){
		$className = 'controllers/'. $className;
	} else {
		$className = str_replace('_','/', $className);
	}
	$className.='.php';
	// echo $className;

	//Check file
    if(file_exists($className))
    {
        include_once $className;
    } else {
    	include_once('public/404.php'); 
    }
}
try{
	$controllerName = isset($_GET['c']) ? $_GET['c'] : 'login';
	$controllerName = ucfirst($controllerName).'Controller';
	$actionName =  isset($_GET['a']) ? $_GET['a'] : 'loginFB';
	$controller = new $controllerName();
	if(!method_exists($controller, $actionName)){
		include_once('public/404.php'); die;	
	}

	// Hiển thị view từ controller
	echo $controller->$actionName();

}catch(\Exception $e){
	echo $e->getMessage();
}
