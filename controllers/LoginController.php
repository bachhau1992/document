<?php 
// Facebook
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

// PHPMailer
// use PHPMailer\PHPMailer\PHPMailer; tương đương với 3 require PHPMailer, SMTP, POP3
// use PHPMailer\PHPMailer\Exception;
require('PHPMailer\src\PHPMailer.php');
require('PHPMailer\src\SMTP.php');
require('PHPMailer\src\POP3.php');
require('PHPMailer\src\OAuth.php');
require('PHPMailer\src\Exception.php');

class LoginController extends BaseController {
	// Login Admin
	public function show () {
		return $this->render('login/login.php');
	}

	public function login() {
		if (isset($_SESSION['errors-login'])) {
			unset($_SESSION['errors-login']);
		}
		// var_dump($_POST); die();
		$isValid = new Validators_Login(); 
		if ($isValid->validate($_POST) == false) {
			$this->setSession('errors-login', $isValid->errors());
			return $this->redirect(['c' => 'login', 'a' => 'show']);
		} 

		$admin = new Models_Admin();

		$email = $_POST['email'];
		$password = md5($_POST['password']); 

		$user = $admin->checkLogin($email, $password);
		// var_dump($user); die;
		if (!empty($user)) {
			$this->setSession('admin_user', $user);
			// var_dump($_SESSION['admin_user']); die;
			if (isset($_SESSION['email'])) {
				unset($_SESSION['email']);
			}
			
			return $this->redirect(['c' => 'users', 'a' => 'index']);
		}
		// Login failed
		$_SESSION['errors-login']['login'] = 'Email or password is wrong.';
		$_SESSION['email'] = $email;
		return $this->redirect(['c' => 'login', 'a' => 'show']);
	
	}

	public function logout() {
		unset($_SESSION['admin_user']);
		return $this->redirect(['c' => 'login', 'a' => 'show']);
	}

	// Login Facebook
	public function loginFB() {
		$appId         = '192669024787850'; //Facebook App ID
		$appSecret     = 'e83f6755b66f33a3ca35bc7e873c63da'; //Facebook App Secret
		$redirectURL   = 'https://training.com/'; //Callback URL
		$fbPermissions = array('email');  //Optional permissions
		

		$fb = new Facebook([
		    'app_id' => $appId,
		    'app_secret' => $appSecret,
		    'default_graph_version' => 'v2.10',
		]);

		// var_dump($fb); die;

		// Get redirect login helper
		$helper = $fb->getRedirectLoginHelper();
		// Get login url
    	$loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);

    	// Try to get access token
    	try {
		    if (isset($_SESSION['facebook_access_token'])) {
		        $accessToken = $_SESSION['facebook_access_token'];
		    } else {
		        $accessToken = $helper->getAccessToken();
		    }
		} catch(FacebookResponseException $e) {
     		echo 'Graph returned an error: ' . $e->getMessage();
      		exit;
		} catch(FacebookSDKException $e) {
		    echo 'Facebook SDK returned an error: ' . $e->getMessage();
      		exit;
		}

		$model = new Models_User();

		// echo $accessToken; die;

		// Check fb token
		if (isset($accessToken)) {
			if (isset($_SESSION['facebook_access_token'])) {
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			} else {
				// Đặt session cho token ngắn hạn
				$this->setSession('facebook_access_token', (string) $accessToken);
				// OAuth 2.0 client handler helps to manage access tokens
				$oAuth2Client = $fb->getOAuth2Client();

				// Thay đổi token ngắn hạn sang dài hạn
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
				$this->setSession('facebook_access_token', (string) $longLivedAccessToken);
			}

			 // Redirect the user back to the same page if url has "code" parameter in query string
		    if(isset($_GET['code'])){
		        header("location:index.php?c=login&a=loginFB");
		    }

		    // Lấy thông tin người dùng
		    try {
		    	$profileRequest = $fb->get('/me?fields=name, email, first_name, last_name, picture');
		    	$fbUserProfile = $profileRequest->getGraphNode()->asArray();
		    } catch (FacebookResponseException $e) {
		    	echo 'Graph returned an error: ' . $e->getMessage();
		        session_destroy();
		        exit;
		    } catch(FacebookSDKException $e) {
		        echo 'Facebook SDK returned an error: ' . $e->getMessage();
		        exit;
		    }

		    // Lấy url logout FB
		    $logoutURL = $helper->getLogoutUrl($accessToken, $redirectURL.'index.php?c=login&a=logoutFB');
		    $_SESSION['logoutURL'] = $logoutURL;

		    // Get user data
		    $fbUserData = [
		    	'name' => $fbUserProfile['first_name'].' '.$fbUserProfile['last_name'],
		    	'facebook_id' => $fbUserProfile['id'],
		    	'email' => $fbUserProfile['email'],
		    	'picture' => $fbUserProfile['picture']['url']
		    ];

		    // Check account
		    $checkAccount = $model->checkAccountFB($fbUserData['facebook_id']);

		    $this->setFlash('user_fb', $fbUserData);

		    if ($checkAccount === true) {
		    	// Check status
		    	$checkStatus = $model->checkStatus($fbUserData['facebook_id']);

		    	if ($checkStatus === true) {
		    		return $this->redirect(['c' => 'users', 'a' => 'info']);
		    	}
				unset($_SESSION['user_fb']);
				$this->setFlash('errors-loginFB', 'Account is block.');	    	
		    } else {
		    	return $this->redirect(['c' => 'login', 'a' => 'registerFB']);
		    }
		}

		return $this->render('login/loginFB.php', ['loginURL' => $loginURL]);
	}	
	
	// Don't have account
	public function registerFB () {
		$info = ($this->hasFlash('user_fb')) ? $_SESSION['user_fb'] : [];
		return $this->render('login/registerFB.php', ['info' => $info]);
	}

	public function addFB () {
		if ($this->hasFlash('user_fb')) unset($_SESSION['user_fb']);

		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$data = [
			'name' => $_POST['name'],
			'email' => $_POST['email'],
			'facebook_id' => $_POST['facebook_id'],
			'status' => '1',
			'ins_id' => '1',
			'upd_id' => '1',
			'ins_datetime' => date('Y-m-d H:i:s'),
			'upd_datetime' => date('Y-m-d H:i:s'),
			'del_flag' => 0
		];

		$model = new Models_User();

		// Nội dung mail
		$body = '<h3>Thông tin người đăng ký:</h3><br>';
		$img = (!empty($_POST['img'])) ? $_POST['img'] : 'http://via.placeholder.com/200x200&text=no-image'; 
		$body .= '<img src="'.$img.'" alt="Avatar"><br>';
		$body .= '<b>Tên: </b>'.$data['name'].'<br>';
		$body .= '<b>Email: </b>'.$data['email'].'<br>';
		$body .= '<b>Facebook ID: </b>'.$data['facebook_id'].'<br>';

		$admin_model = new Models_Admin();
		// Super admin đầu tiên để gửi thông tin
		$superadmin = $admin_model->findSuperAdmin();

		$admin = $admin_model->getList();
		$i = 0; $ccmail = [];
		// Danh sách các admin để cc mail
		foreach ($admin as $ad) {
			$ccmail[$i] = $ad['email'];
			$i ++;
		}

		// var_dump($mailer); echo '<br>'; foreach ($ccmail as $k => $v) {echo $v.'<br>';} die;

		// GỬI MAIL
		// $mail = new PHPMailer(true); nếu dùng use PHPMailer/PHPMailer/PHPMailer;
		$mail = new PHPMailer\PHPMailer\PHPMailer(true);
		try {
			// Thiết lập SMTP
			$mail->isSMTP();
			$mail->CharSet = "utf-8";
			$mail->SMTPDebug = 2;

			// Đăng nhập gmail
			$mail->SMTPAuth = true;

			// Dùng local thì dùng TLS
			// $mail->SMTPSecure = "ssl";
			$mail->SMTPSecure = 'tls';
			$mail->SMTPOptions = array(
			    'ssl' => array(
			        'verify_peer' => false,
			        'verify_peer_name' => false,
			        'allow_self_signed' => true
			    )
			);

			// SMTP của Gmail
			$mail->Host = "smtp.gmail.com";
			// $mail->Port = 465;
			$mail->Port = 587; // Localhost

			// Email người gửi
			$mail->Username = 'hoangpham2395@gmail.com';
			$mail->Password = 'hdeqgnczpbnfwtsj';
			$mail->setFrom($data['email'], $data['name']);
			// 	$mail->From = 'hoangpham2395@gmail.com';
			// 	$mail->FromName = 'Hoang Pham';
		
			// Email người nhận		
		 	$mail->AddAddress($superadmin['email'], $superadmin['name']);
		 	// Địa chỉ CC:
			if (!empty($ccmail)) {
				foreach ($ccmail as $k => $v) {
					$mail->addCC($v);
				} 
			}

			// Nội dung chuẩn bị gửi mail
			$mail->isHTML(true);
			$mail->Subject = 'Đăng ký tài khoản bằng facebook để truy cập vào TrainingPHP'; 		
			$mail->Body = $body;

			$mail->send();

		    // Insert DB
			$user_fb = $model->create($data); 
			if (!$user_fb) {
				$this->setFlash('confirm-loginFB', 'Register failed.');
			}

			return $this->redirect(['c' => 'login', 'a' => 'loginFB']);
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
	}

	public function logoutFB() {
		unset($_SESSION['facebook_access_token']);
		unset($_SESSION['user_fb']);
		return $this->redirect(['c' => 'login', 'a' => 'loginFB']);
	}
}
?>
