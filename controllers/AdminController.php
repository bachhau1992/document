<?php 
class AdminController extends BaseController
{	
	public function role () {
		return $this->render('layouts/role.php', []);
	}

	public function index(){
		$model = new Models_Admin();

		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$this->setFlash('admin_user', $model->find($id));
		}

		// Search 
		$term = []; $search = [];
		if (!empty($_GET['name'])) {
			$term['name'] = '%'.$_GET['name'].'%';
			$search['name'] = $_GET['name'];
		}
		if (!empty($_GET['email'])) {
			$term['email'] = '%'.$_GET['email'].'%';
			$search['email'] = $_GET['email'];
		}

		// Pagination
		$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : 10;
		$page = (!empty($_GET['page'])) ? $_GET['page'] : 1;
		$start = ($page - 1) * $limit;
		$links = 5;
		$list_class = 'pagination pull-right';

		if (!empty($term)) {
			$admin = $model->searchPaginate($term, $limit, $start);
			$total = count($model->search($term));
		} else {
			$admin = $model->paginate($limit, $start);
			$total = count($model->getList());
		}

		$pagination = $model->createLink($limit, $page, $total, $links, $list_class, $search);

		$count = ['showing' => count($admin), 'total' => $total];

		return $this->render('admin/index.php', ['admin'=> $admin, 'pagination' => $pagination, 'count' => $count]);
	} 

	public function create () {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$this->setFlash('admin_user', $model->find($id));
		}

		return $this->render('admin/add.php', []);
	}

	public function add () {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$this->setFlash('admin_user', $model->find($id));
		}

		// Check validate
		$isValid = new Validators_Admin();
		$data = [
			'name' => $_POST['name'],
			'password' => $_POST['password'],
			'password_confirmation' => $_POST['password_confirmation'],
			'email' => $_POST['email'],
			'role_type' => $_POST['role_type']
		];

		// var_dump($data); die;

		if ($isValid->requiredAdmin($data) === false) {
			$this->setFlash('add-admin', $_POST);
			$this->setFlash('errors-addAdmin', $isValid->errors());

			// var_dump($_SESSION['add-admin']); die;
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}
		// Check password length
		if ($isValid->checkPasswordSize($_POST['password']) === false) {
			$this->setFlash('add-admin', $_POST);
			$this->setFlash('errors-addAdmin', $isValid->errors());
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}

		// Check password confirm
		if ($isValid->checkPasswordConfirmation($_POST['password'], $_POST['password_confirmation']) === false) {
			$this->setFlash('errors-addAdmin', $isValid->errors());
			$this->setFlash('add-admin', $_POST);
			// var_dump($_SESSION['errors-addAdmin']); die;
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}

		$model = new Models_Admin();

		// Check unique email 
		if ($model->uniqueEmail($_POST['email']) === false) {
			$this->setFlash('add-admin', $_POST);
			$this->setFlash('errors-addAdmin', ['email' => 'Email is unique.']);
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}

		// var_dump($data); die;

		// Validate success
		date_default_timezone_set('Asia/Ho_Chi_Minh');

		$data['ins_id'] = $_SESSION['admin_user']['id'];
		$data['password'] = md5($_POST['password']);
		$data['avatar'] = null;

		// Avatar
		$errorsImg = [];
		if (!empty($_FILES['avatar']['name'])) {
			$img_name = $_FILES["avatar"]['name'];
			$img_path = "upload/".$img_name;
			// echo $img_name.'<br>'.$img_path; die;
			
			$img_type = strtolower(pathinfo($img_path,PATHINFO_EXTENSION));
			if ($isValid->checkImage($img_type) === false) {
				$errorsImg['type'] = 'Sorry, only JPG, JPEG, PNG, GIF are allowed.';
			} 
			// Giới hạn ảnh 600KB
			if ($isValid->checkFileSize('avatar') === false) {
				$errorsImg['size'] = 'Sorry, your file is too large (< 600KB).';
			}

			if (empty($errorsImg)) {
				$img_name = date('YmdHis').".".$img_type;
				$img_path = 'uploads/'.$img_name;
				
				if (move_uploaded_file($_FILES['avatar']['tmp_name'], "$img_path")) {
					$data['avatar'] = $img_name;
				} else {
					$errorsImg['upload'] = "Sorry, there was an error uploading your avatar.";
				}
			}
		} 

		if (!empty($errorsImg)) {
			$this->setSession('errors-addAdmin', $errorsImg);
			$this->setFlash('add-admin', $_POST);
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		} 

		$admin = $model->create($data); 

		if ($admin) {
			$this->setFlash('admin', 'Add new admin success!');
			return $this->redirect(['c' => 'admin', 'a' => 'index']);
		} else {
			$this->setSession('create-user', $admin);
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}
	}

	public function show() {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$model = new Models_Admin();
			$admin = $model->find($id);
			
			return $this->render('admin/show.php', ['admin' => $admin]);
		} else {
			include_once('public/404.php'); die;
		}
	}

	public function edit () {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$model = new Models_Admin();
			$admin = $model->find($id);
			return $this->render('admin/edit.php', ['admin' => $admin]);
		} else {
			include_once('public/404.php'); die;
		}
	}

	public function update () {
		$model = new Models_Admin();

		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$this->setFlash('admin_user', $model->find($id));
		}

		$id = $_GET['id'];

		// Validate
		$isValid = new Validators_Admin();
		$data = [];

		// Get data
		$data['id'] = $id;
		$data['name'] = $_POST['name'];
		$data['email'] = $_POST['email'];
		$data['role_type'] = $_POST['role_type'] ;

		if ($isValid->requiredAdmin($data) === false) {
			$this->setFlash('update-admin', $_POST);
			$this->setFlash('errors-updateAdmin', $isValid->errors());
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		}

		// Check unique email 
		if ($model->uniqueEditEmail($id, $_POST['email']) === false) {
			$this->setFlash('update-admin', $_POST);
			$this->setFlash('errors-updateAdmin', ['email' => 'Email is unique.']);
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		}

		// Validate success
		$admin = $model->find($id);
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$data['avatar'] = $admin['avatar'];
		$data['upd_id'] = $_POST['upd_id'];

		// Check image
		$errorsImg = [];
		if (!empty($_FILES['avatar']['name'])) {
			$img_name = $_FILES["avatar"]['name'];
			$img_path = "upload/".$img_name;
			
			$img_type = strtolower(pathinfo($img_path,PATHINFO_EXTENSION));

			if (!empty($img_type) && $isValid->checkImage($img_type) === false) {
				$errorsImg['type'] = 'Sorry, only JPG, JPEG, PNG, GIF are allowed.';
			} 
			if ($isValid->checkFileSize('avatar') === false) {
				$errorsImg['size'] = 'Sorry, your file is too large (600KB).';
			}

			if (empty($errorsImg)) {
				$img_name = date('YmdHis').".".$img_type;
				$img_path = 'uploads/'.$img_name;
				
				// Move file upload from tmp to uploads
				if (move_uploaded_file($_FILES['avatar']['tmp_name'], "$img_path")) {
					$data['avatar'] = $img_name;
					// Delete old avatar
					if (!empty($admin['avatar']) && file_exists('uploads/'.$admin['avatar'])) {
						unlink('uploads/'.$admin['avatar']);
					}
					// To change avatar in sidebar
					if ($_SESSION['admin_user']['id'] == $id) {
						$_SESSION['admin_user']['avatar'] = $img_name;
					}
				} else {
					$errorsImg['upload'] = "Sorry, there was an error uploading your avatar.";
				}
			}
		} 

		if (!empty($errorsImg)) {
			$this->setFlash('update-admin', $_POST);
			$this->setSession('errors-updateAdmin', $errorsImg);
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		} 

		// Check password
		if (!empty($_POST['old_password'])) {
			$old_password = md5($_POST['old_password']);
			if ($model->checkOldPassword($old_password) === false) {
				$this->setFlash('errors-updateAdmin', ['old_password' => 'Old password is wrong.']);
			} elseif (empty($_POST['new_password']) || empty($_POST['password_confirmation'])){
				$this->setFlash('errors-updateAdmin', ['required_password' => "New password or password confirmation can't be blank."]);
			} elseif ($isValid->checkPasswordSize($_POST['new_password']) === false) {
				 $this->setFlash('errors-updateAdmin', ['new_password_length' => 'Minimum password length is 6.']);
			} elseif ($isValid->checkPasswordConfirmation($_POST['new_password'], $_POST['password_confirmation']) === false) {
				$this->setFlash('errors-updateAdmin', ['password_confirmation' => "Those password didn't match. Try again."]);
			}else {
				$data['password'] = md5($_POST['new_password']);
			}
		} else {
			if (!empty($_POST['new_password']) || !empty($_POST['password_confirmation'])) {
				$this->setFlash('errors-updateAdmin', ['old_password' => "You don't fill old password field."]);
			}
		}

		// Password is not valid.
		if (!empty($_SESSION['errors-updateAdmin'])) {
			$this->setFlash('update-admin', $_POST);
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		}

		// Update admin
		$admin = $model->update($id, $data);
		if ($admin) {
			$this->setFlash('admin', 'Update success!');

			// Check session admin_user
			if ($_SESSION['admin_user']['id'] == $id) {
				$this->setFlash('admin_user', $data);
			} 

			return $this->redirect(['c' => 'admin', 'a' => 'index']);
		} 

		// Update failed.
		$this->setFlash('errors-updateAdmin', ['update' => 'Update failed.']);
	}

	public function destroy () {
		if (isset($_GET['id'])) {
			$model = new Models_Admin();

			// Check session admin_user
			if ($this->hasFlash('admin_user')) {
				$id = $_SESSION['admin_user']['id'];
				$this->setFlash('admin_user', $model->find($id));
			}

			$admin = $model->find($_GET['id']);

			// Xóa avtar
			if (!empty($admin['avatar']) && file_exists('uploads/'.$admin['avatar'])) {
				unlink('uploads/'.$admin['avatar']);
			}

			$admin = $model->delete($_GET['id']);
			$this->setFlash('admin', 'Delete success!');

			// Check session admin_user
			if ($_SESSION['admin_user']['id'] == $_GET['id']) {
				unset($_SESSION['admin_user']);
			}

			return $this->redirect(['c' => 'admin', 'a' => 'index']);
		} else {
			include_once('public/404.php'); die;
		}
	}
}
