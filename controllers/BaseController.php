<?php
class BaseController
{	
	// Get view
	public function render ($viewPath, $data = [])
	{
		ob_start();
		extract($data);
		include_once 'views/'.$viewPath;
		$result = ob_get_contents();
		ob_end_clean();
		return $result;
	}

	// Redirect to other page
	public function redirect ($params = []) {
		$link = http_build_query($params);
		// return $link;
		header("location:index.php?".$link);
	}
	
	// Set flash
	public function setFlash ($session, $content) {
		return $_SESSION[$session] = $content;
	}
	// Get flash 
	public function getFlash ($session) {
		$get = $_SESSION[$session];
		unset($_SESSION[$session]);
		return $get;
	}
	// Check flash
	public function hasFlash ($session) {
		return (isset($_SESSION[$session])) ? true : false;
	}

	public function getSession ($session) {
		return $_SESSION[$session];
	}

	public function setSession ($session, $content) {
		return $_SESSION[$session] = $content;
	}
}

?>
