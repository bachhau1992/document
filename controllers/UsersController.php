<?php class UsersController extends BaseController{
	public function index()	{
		$model = new Models_User();

		// Get term
		$term = []; $search = [];
		if (!empty($_GET['name'])) {
			$term['name'] = '%'.$_GET['name'].'%';
			$search['name'] = $_GET['name'];
		}

		if (!empty($_GET['email'])) {
			$term['email'] = '%'.$_GET['email'].'%';
			$search['email'] = $_GET['email'];
		}

		// Pagination and display
		$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : 10;
		$page = (!empty($_GET['page'])) ? $_GET['page'] : 1;
		$start = ($page - 1) * $limit;
		$links = 3;
		$list_class = 'pagination pull-right';

		if (!empty($term)) {
			$users = $model->searchPaginate($term, $limit, $start);
			$total = count($model->search($term));
		} else {
			$users = $model->paginate($limit, $start);
			$total = count($model->getList());
		}

		$pagination = $model->createLink($limit, $page, $total, $links, $list_class, $search);

		$count = ['showing' => count($users), 'total' => $total];

		return $this->render('users/index.php', ['users'=> $users, 'pagination' => $pagination, 'count' => $count]);
	} 

	public function edit () {
		if (isset($_GET['id'])) {
			$model = new Models_User();
			$user = $model->find($_GET['id']); 
			
			return $this->render('users/edit.php', ['user' => $user]);
		} else {
			include_once('public/404.php'); die;
		}
	}

	public function update() {
		$id = $_GET['id'];

		$_POST['upd_id'] = $_SESSION['admin_user']['id'];
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$_POST['upd_datetime'] = date('Y-m-d H:i:s');
		// var_dump($_POST); die;

		// Check validate
		$isValid = new Validators_User();
		if ($isValid->requiredUser($_POST) === false) {
			// var_dump($_POST); die; 
			$this->setFlash('errors-editUser', $isValid->errors());
			// var_dump($_SESSION['errors-user']); die;
			$this->setFlash('edit-user', $_POST);
			return $this->redirect(['c' => 'users', 'a' => 'edit', 'id' => $id]);
		} 

		$model = new Models_User();

		if ($model->uniqueEditEmail($id, $_POST['email']) === false) {
			$this->setFlash('errors-editUser', ['uniqueEmail' => 'Email is unique.']);
			$this->setFlash('edit-user', $_POST);
			return $this->redirect(['c' => 'users', 'a' => 'edit', 'id' => $id]);
		}
		
		if (isset($_SESSION['edit-user'])) unset($_SESSION['edit-user']);
		
		
		$user = $model->update($id, $_POST);
		// var_dump($user); die;

		// Update success
		if ($user) {
			$this->setFlash('users', 'Update success!');
			return $this->redirect(['c' => 'users', 'a' => 'index']);
		} 

		// Update failed
		$this->setSession('errors-user', 'Update failed.');
		return $this->redirect(['c' => 'users', 'a' => 'edit', 'id'=> $id]);
	}

	public function destroy() {
		$id = $_GET['id'];
		$model = new Models_User();
		$user = $model->delete($id); echo $user;

		if ($user) {
			$this->setFlash('users', "Delete success!");
			return $this->redirect(['c' => 'users', 'a' => 'index']);
		} else {
			echo 'false'; die();
		}
		return $this->redirect(['c' => 'users', 'a' => 'index']);	
	}

	public function info () {
		if ($this->hasFlash('user_fb')) {
			$info = $_SESSION['user_fb'];
			$info['logoutURL'] = (isset($_SESSION['logoutURL'])) ? $_SESSION['logoutURL'] : 'index.php?c=login&a=logoutFB'; 
			return $this->render('users/info.php', ['info' => $info]);
		} else {
			return $this->redirect(['c' => 'login', 'a' => 'loginFB']);
		}
	}
}
