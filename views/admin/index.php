<?php 
if (isset($_SESSION['admin_user'])) {
	if ($_SESSION['admin_user']['role_type'] == 1) {
		require 'views/layouts/top.php';	
?>

<script type="text/javascript">
	// Confirm xóa admin
	function deleteAdmin() {
		return confirm('Are you sure?');
	}

	// Thay đổi limit phần tử trong 1 trang
	function limitChanged(obj) {
		var url;
		var limit = obj.value;
		var currentURL = window.location.toString();
		if (currentURL.indexOf("&page") > 0) {
			// Cắt từ vì tìm thấy đến hết
			page = currentURL.slice(currentURL.indexOf("&page"));
			url1 = currentURL.slice(0, currentURL.indexOf("&page"));
		} else {
			page = '';
			url1 = currentURL;
		}
		if (url1.indexOf("&limit") > 0) {
			// Cắt từ đầu đến vị trí tìm thấy
			url2 = url1.slice(0, url1.indexOf("&limit"));
		} else {
			url2 = url1; 
		}
		url = url2+'&limit='+limit+''+page;
		// document.write(url);
		window.location.href = url;
		// window.location.href = currentURL+'&limit='+limit;
	}
</script>
			
<section class="content-header">
	<h1>Admin <small>List of Admin</small></h1>
</section>

<?php 
if ($this->hasFlash('admin')) {
	?>
	<div class="alert alert-success">
		<ul>
		<?php
		$notifies [] = $this->getFlash('admin');
		foreach ($notifies as $notify) {
			echo '<li>'.$notify.'</li>';
		}
		?>
		</ul>
	</div>
	<?php
}
?>

<!-- Content body -->
<section class="content">
	<div class="box">
		<div class="box-header"><span>Search admin</span></div>
		<div class="box-body">
			<form method="GET">
				<input type="hidden" name="c" value="admin">
				<input type="hidden" name="a" value="index">
				<div class="form-group width50">
					<label for="name">Name:</label><br>
					<input type="input" name="name" class="form-control" value="<?php echo (isset($_GET['name'])) ? $_GET['name'] : ''; ?>">
				</div>
				<div class="form-group width50">
					<label for="email">Email:</label><br>
					<input type="text" name="email" class="form-control" value="<?php echo (isset($_GET['email'])) ? $_GET['email'] : ''; ?>">
				</div>

				<?php 
				if (isset($_GET['limit'])) {
					?>
					<input type="hidden" name="limit" value="<?php echo $_GET['limit'];?>">	
					<?php
				}
				?>

				<div class="form-group width100 text-center">
					<button type="submit" class="btn btn-danger">Search</button>
					<button type="reset" class="btn btn-primary">Reset</button>
				</div>
			</form>
			<div class="clear"></div>
		</div>		
	</div>

	<div class="box">
		<div class="box-header"><span>List of Admin</span></div>
		<div class="box-body">
			<button type="button" class="btn-a btn-success" style="margin-bottom: 15px;"><a href="index.php?c=admin&a=index">Reload</a></button>
			<div class="pull-right">
				Show
				<select id="limit" onchange="limitChanged(this)" style="height: 35px;">
					<option value="10" <?php if (isset($_GET['limit']) && $_GET['limit'] == 10) echo 'selected';?> >10</option>
					<option value="25" <?php if (isset($_GET['limit']) && $_GET['limit'] == 25) echo 'selected';?> >25</option>
					<option value="50" <?php if (isset($_GET['limit']) && $_GET['limit'] == 50) echo 'selected';?> >50</option>
				</select>
				entries
			</div>
			<table id="myTable" class="table">
				<thead>
					<th width="5%">No.</th>
					<th width="20%" onclick="sortTable(1)">Name <span>&uarr;&darr;</span></th>
					<th width="35%" onclick="sortTable(2)">Email <span>&uarr;&darr;</span></th>
					<th width="20%" onclick="sortTable(3)">Role <span>&uarr;&darr;</span></th>
					<th width="5%">Show</th>
					<th width="5%">Edit</th>
					<th width="5%">Delete</th>
				</thead>
				
				<tbody>
					<?php 
					if (empty($admin)) {
						?>
						<tr>
							<td colspan="7" class="text-center red"><i class="fa fa-exclamation-triangle"></i> Not data</td>
						</tr>
						<?php
					}
					$no = 1; 
					foreach ($admin as $a) {
					?>
						<tr>
							<td class="text-center"><?php echo $no;?></td>
							<td><?php echo $a['name'];?></td>
							<td><?php echo $a['email'];?></td>
							<td><?php echo ($a['role_type'] == 1) ? "Super admin" : "Admin";?></td>
							<td class="text-center"><button type="button" class="btn-a btn-primary"><a href="index.php?c=admin&a=show&id=<?php echo $a['id'];?>"><i class="fa fa-th-list"></i></a></button></td>
							<td class="text-center"><button type="button" class="btn-a btn-success"><a href="?c=admin&a=edit&id=<?php echo $a['id'];?>"><i class="fa fa-pencil"></i></a></button></td>
							<td class="text-center">
								<?php if ($a['role_type'] == 2) { ?>
								<button type="button" class="btn-a btn-danger"><a href="?c=admin&a=destroy&id=<?php echo $a['id'];?>" onclick="deleteAdmin()"><i class="fa fa-trash"></i></a></button>
								<?php } ?>
							</td>
						</tr>
						<?php 
						$no ++;
					}
					?>
				</tbody>
			</table>
			<span class="pull-left margin-top-15"><?php echo 'Showing '.$count['showing'].' of '.$count['total'].' entries'; ?></span>
			<?php if (!empty($pagination)) echo $pagination; ?>
			<div class="clear"></div>
		</div>
	</div>
</section>	

<!-- Sort table -->
<script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  /* switching: true-chuyển đổi, false-không muốn chuyển đổi.
   * shouldSwitch: true-nên chuyển đổi, false-không nên chuyển đổi.
   * ->chuyển đổi: đổi vị trí x và y cho nhau.
   * dir: cách sắp xếp (asc: tăng dần, desc: giảm dần). 
   * swithchcount: số lần chuyển đổi. */ 

  table = document.getElementById("myTable");
  switching = true;
  //asc: Sắp xếp tăng dần
  dir = "asc"; 
  /* Vòng lặp while để sắp xếp, khi sắp xếp xong thì switching = false */
  while (switching) {
    // Ban đầu đặt switching = false để tránh vòng lặp vô hạn
    switching = false;
    rows = table.getElementsByTagName("TR");
    /* Vòng lặp qua tất cả các hàng trừ hàng đầu tiên (TH) */
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Lấy 2 phần tử muốn so sánh, x là hàng hiện tại, y là hàng kế tiếp tại cột n, n bắt đầu từ 0*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*Check 2 hàng đó  có cần đổi vị trí cho nhau k dựa vào biến dir (ban đầu là asc)*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // Nếu true thì x phải xếp sau y theo asc -> đặt nên chuyển đổi
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // Nếu true thì x phải xếp sau y theo desc -> đặt nên chuyển đổi
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* shouldSwitch = true -> nên chuyển đổi -> chuyển đổi vị trí i với i+1 cho nhau */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      // Đặt lại switching để bắt đầu vòng lặp mới xem có cần phải chuyển đổi nữa không
      switching = true;
      // Tăng biến đếm số lần chuyển đổi
      switchcount ++;      
    } else {
      /* Tức đã sắp xếp xong theo hướng asc, đặt lại theo hướng desc và thực hiện vòng lặp tiếp */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>

<?php
		require 'views/layouts/bottom.php';
	} else {
		header("location:index.php?c=admin&a=role");
	}
} else {
	header("location:index.php?c=login&a=show");
}
?>
