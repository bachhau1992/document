<?php 
if (isset($_SESSION['admin_user'])) {
	if ($_SESSION['admin_user']['role_type'] == 1 || $_SESSION['admin_user']['id'] == $_GET['id']) {
		require 'views/layouts/top.php';
?>

<script type="text/javascript">
	function deleteAdmin() {
		return confirm('Are you sure?');
	}
</script>
			
<section class="content-header">
	<h1>Admin <small>Infomation</small></h1>
</section>

<!-- Content body -->
<section class="content">
	<div class="box">
		<div class="box-header"><span>Infomation</span></div>
		<div class="box-body">
			<?php  
			if (empty($admin)) {
				echo 'Not found, <a href="index.php?c=admin&a=index">return</a>.';
			} else {
			?>
				<div class="form-group width30">
					<?php  
					if (!empty($admin['avatar']) && file_exists('uploads/'.$admin['avatar'])) {
						?>
						<img src="uploads/<?php echo $admin['avatar'];?>" style="width: 150px; height: 150px;">
						<?php
					} else {
						?>
						<img src="public/images/no-image.png" class="img-show" alt="Avatar">
						<?php
					}
					?>
				</div>
				<div class="form-group width50">
					<div class="margin-top-15"></div>
					<span>Name: <b><?php echo $admin['name'];?></b></span>
					<div class="margin-top-15"></div>
					<span>Email: <b><?php echo $admin['email'];?></b></span>
					<div class="margin-top-15"></div>
					<span>Role: <b><?php echo ($admin['role_type'] == 1) ? "Super admin" : "Admin";?></b></span><br>
					<div class="margin-top-15"></div>
					<button type="button" class="btn-a btn-primary"><a href="?c=admin&a=edit&id=<?php echo $admin['id'];?>"><i class="fa fa-pencil"></i></a></button> 
					<?php if ($admin['role_type'] == 2) { ?>
						<button type="button" class="btn-a btn-danger"><a href="?c=admin&a=destroy&id=<?php echo $admin['id'];?>" onclick="deleteAdmin()"><i class="fa fa-trash"></i></a></button>
					<?php } ?>
					<button type="button" class="btn-a btn-success"><a href="?c=admin&a=index"><i class="fa fa-chevron-circle-left"></i></a></button>
				</div>
				<?php 
			}
			?>
		</div>	
		<div class="clear"></div>	
	</div>
</section>			
			
<?php
		require 'views/layouts/bottom.php';
	} else {
			header("location:index.php?c=admin&a=role");
	}
} else {
	header("location:index.php?c=login&a=show");
}
?>