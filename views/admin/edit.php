<?php 
if (isset($_SESSION['admin_user'])) {
	if ($_SESSION['admin_user']['role_type'] == 1 || $_SESSION['admin_user']['id'] == $_GET['id']) {
		require 'views/layouts/top.php';
?>
			
<section class="content-header">
	<h1>Admin <small>Edit information admin</small></h1>
</section>

<?php  
	if ($this->hasFlash('errors-updateAdmin')) {
		?>
		<div class="alert alert-danger">
			<ul>
			<?php
			$errors = $this->getFlash('errors-updateAdmin');
			foreach ($errors as $error) {
				echo '<li>'.$error.'</li>';
			}
			?>
			</ul>
		</div>
		<?php
	}
?>

<!-- Content body -->
<section class="content">
	<div class="box">
		<div class="box-header"><span>Edit information admin</span></div>
		<div class="box-body">
			<?php  
			if (empty($admin)) {
				echo 'Not found, <a href="index.php?c=admin&a=index">return</a>.';
			} else {
			?>
				<form method="POST" action="index.php?c=admin&a=update&id=<?php echo $admin['id'];?>"" enctype="multipart/form-data">	
					<input type="hidden" name="upd_id" value="<?php echo $_SESSION['admin_user']['id'];?>">
					<?php 
						$oldValues = [];
						if (isset($_SESSION['update-admin'])) {
							$oldValues [] = $this->getFlash('update-admin');
							// var_dump($oldValues); die;
							// echo $oldValues[0]['name']; die;
						}
					?>			
					<div class="form-group width50">
						<label for="name">Name: <span class="red"> &#42; </span></label><br>
						<input type="input" name="name" class="form-control" value="<?php echo (!empty($oldValues[0]['name'])) ? $oldValues[0]['name'] : $admin['name']; ?>">
					</div>
					<div class="form-group width50">
						<label for="old_password">Old password: </label><br>
						<input type="password" name="old_password" class="form-control">
					</div>
					<div class="form-group width50">
						<label for="email">Email: <span class="red"> &#42; </span></label><br>
						<input type="email" name="email" class="form-control" value="<?php echo (!empty($oldValues[0]['email'])) ? $oldValues[0]['email'] : $admin['email']; ?>">
					</div>
					<div class="form-group width50">
						<label for="password">New password: </label><br>
						<input type="password" name="new_password" class="form-control">
					</div>
					<div class="form-group width50">
						<label for="role_type">Role: <span class="red"> &#42; </span></label><br>
						<select name="role_type">
							<option value="">--- Select status ---</option>
							<?php 
							$super = ''; $se_admin = '';
							if (($admin['role_type'] == 1) || (!empty($oldValues[0]['role_type'] && $oldValues[0]['role_type'] == 1))) {
								$super = 'selected';
								$se_admin = '';
							} elseif (($admin['role_type'] == 2) || (!empty($oldValues[0]['role_type'] && $oldValues[0]['role_type'] == 2))) {
								$super = '';
								$se_admin = 'selected'; 
							}
							?>
							<option value="1" <?php echo $super; ?> >Super admin</option>
							<option value="2" <?php echo $se_admin; ?> >Admin</option>
						</select>
					</div>
					<div class="form-group width50">
						<label for="password_confirmation">Password confirmation: </label><br>
						<input type="password" name="password_confirmation" class="form-control">
					</div>
					<div class="form-group width50">
						<label for="avatar">Avatar:</label><br>
						<input type="file" name="avatar" class="form-control">
					</div>
					<input type="hidden" name="upd_id" value="<?php echo $_SESSION['admin_user']['id']; ?>">
			
					<div class="form-group width100 text-center">
						<button type="submit" name="save" class="btn btn-danger" value="editUser">Save</button>
						<button type="button" class="btn-a btn-primary"><a href="index.php?c=admin&a=index">Cancel</a></button>
					</div>
				</form>
				<?php 
			}
			?>
			<div class="clear"></div>
		</div>		
	</div>
</section>			
			
<?php
		require 'views/layouts/bottom.php';
	} else {
		header("location:index.php?c=admin&a=role");
	}
} else {
	header("location:index.php?c=login&a=show");
}
?>
