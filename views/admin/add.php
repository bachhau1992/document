<?php 
if (isset($_SESSION['admin_user'])) {
	if ($_SESSION['admin_user']['role_type'] == 1) {
		require 'views/layouts/top.php';
?>
			
<section class="content-header">
	<h1>Admin <small>Add new admin</small></h1>
</section>

<?php  
	if (isset($_SESSION['errors-addAdmin'])) {
		?>
		<div class="alert alert-danger">
			<ul>
			<?php
			$errors = $this->getFlash('errors-addAdmin');
			foreach ($errors as $error) {
				echo '<li>'.$error.'</li>';
			}
			?>
			</ul>
		</div>
		<?php
	}
?>

<!-- Content body -->
<section class="content">
	<div class="box">
		<div class="box-header"><span>Add new admin</span></div>
		<div class="box-body">
			<form method="POST" action="index.php?c=admin&a=add" enctype="multipart/form-data">	
				<input type="hidden" name="ins_id" value="<?php echo $_SESSION['admin_user']['id'];?>">
				<?php 
					$oldValues = [];
					if (isset($_SESSION['add-admin'])) {
						$oldValues [] = $this->getFlash('add-admin');
						// $oldValues = extract($oldValues);
						// var_dump($oldValues); die;
						// echo $oldValues[0]['name']; die;
					}
				?>			
				<div class="form-group width50">
					<label for="name">Name: <span class="red"> &#42; </span></label><br>
					<input type="input" name="name" class="form-control" value="<?php echo (!empty($oldValues[0]['name'])) ? $oldValues[0]['name'] : ''; ?>">
				</div>
				<div class="form-group width50">
					<label for="password">Password: <span class="red"> &#42; </span></label><br>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="form-group width50">
					<label for="password_confirmation">Password confirmation: <span class="red"> &#42; </span></label><br>
					<input type="password" name="password_confirmation" class="form-control">
				</div>
				<div class="form-group width50">
					<label for="email">Email: <span class="red"> &#42; </span></label><br>
					<input type="email" name="email" class="form-control" value="<?php echo (!empty($oldValues[0]['email'])) ? $oldValues[0]['email'] : ''; ?>">
				</div>
				<div class="form-group width50">
					<label for="avatar">Avatar:</label><br>
					<input type="file" name="avatar" class="form-control">
				</div>
				<div class="form-group width50">
					<label for="role_type">Role: <span class="red"> &#42; </span></label><br>
					<select name="role_type">
						<option value="">--- Select status ---</option>
						<option value="1" <?php echo (!empty($oldValues[0]['role_type']) && ($oldValues[0]['role_type'] == 1)) ? 'selected' : ''; ?> >Super admin</option>
						<option value="2" <?php echo (!empty($oldValues[0]['role_type']) && ($oldValues[0]['role_type'] == 2)) ? 'selected' : ''; ?> >Admin</option>
					</select>
				</div>
		
				<div class="form-group width100 text-center">
					<button type="submit" name="save" class="btn btn-danger" value="editUser">Save</button>
					<button type="button" class="btn-a btn-primary"><a href="index.php?c=admin&a=index">Cancel</a></button>
				</div>
			</form>
			<div class="clear"></div>
		</div>		
	</div>
</section>			
			
<?php
		require 'views/layouts/bottom.php';
	} else {
			header("location:index.php?c=admin&a=role");
	}
} else {
	header("location:index.php?c=login&a=show");
}
?>
