<section class="sidebar">
	<!-- User -->
	<div class="user-panel">
		<!-- Avatar -->
		<div class="pull-left image">
			<a href="index.php?c=admin&a=show&id=<?php echo $_SESSION['admin_user']['id'];?>">
				<?php  
				if (!empty($_SESSION['admin_user']['avatar']) && file_exists('uploads/'.$_SESSION['admin_user']['avatar'])) {
					?>
					<img src="<?php echo 'uploads/'.$_SESSION['admin_user']['avatar'];?>" class="img-cirle" alt="Avatar">
					<?php
				} else {
					?>
					<img src="public/images/no-image.png" class="img-cirle" alt="Avatar">
					<?php 
				}
				?>
			</a>
		</div>

		<!-- Information -->
		<div class="pull-left info">
			<p><a href="index.php?c=admin&a=show&id=<?php echo $_SESSION['admin_user']['id'];?>"><?php echo (!empty($_SESSION['admin_user']['name'])) ? $_SESSION['admin_user']['name'] : "Unknow name"; ?></a></p>
			<span>&#9679;</span> Online
		</div>

		<div class="clear"></div>
	</div>

	<!-- Menu -->
	<ul class="sidebar-menu">
		<li id="treeview" onclick="clickMenu()">
			<a href="">
				<span><i class="fa fa-user"></i> Admin</span>
			</a>
			<span class="pull-right" style="margin-right: 15px;"><i class="fa fa-angle-down"></i></span>
		</li>
		<ul id="treeview-menu">
			<li><a href="index.php?c=admin&a=index"><i class="fa fa-th-list"></i> List of admins</a></li>
			<li><a href="index.php?c=admin&a=create"><i class="fa fa-plus-circle"></i> Add new admin</a></li>
		</ul>
		<li>
			<a href="?c=users&a=index"><i class="fa fa-users"></i> Users</a>
		</li>
	</ul>
</section>

<script type="text/javascript">
	function clickMenu () {
		var x = document.getElementById('treeview-menu');
		if (x.style.display === "block") {
			x.style.display = "none";
		} else {
			x.style.display = "block";
		}
	}
</script>
