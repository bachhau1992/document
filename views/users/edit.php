<?php 

if (isset($_SESSION['admin_user'])) {
	require 'views/layouts/top.php';
?>
			
<section class="content-header">
	<h1>Users <small>Edit user</small></h1>
</section>

<?php  
	if ($this->hasFlash('errors-editUser')) {
		?>
		<div class="alert alert-danger">
			<ul>
				<?php	
				$errors = $this->getFlash('errors-editUser');
				foreach ($errors as $error) {
					echo '<li>'.$error.'</li>';
				}
				?>
			</ul>
		</div>
		<?php
	}
?>

<!-- Content body -->
<section class="content">
	<div class="box">
		<div class="box-header"><span>Edit user</span></div>
		<div class="box-body">
			<?php  
			if (empty($user)) {
				echo 'Not found, <a href="index.php?c=users&a=index">return</a>.';
			} else {
			?>
				<form method="POST" action="index.php?c=users&a=update&id=<?php echo $user['id'];?>">				
					<div class="form-group width50">
						<label for="name">Name: <span class="red"> &#42; </span></label><br>
						<input type="input" name="name" class="form-control" value="<?php echo $user['name']; ?>">
					</div>
					<div class="form-group width50">
						<label for="email">Email: <span class="red"> &#42; </span></label><br>
						<input type="text" name="email" class="form-control" value="<?php echo $user['email']?>">
					</div>
					<div class="form-group width50">
						<label for="facebook_id">Facebook ID: <span class="red"> &#42; </span></label><br>
						<input type="text" name="facebook_id" class="form-control" value="<?php echo $user['facebook_id'];?>">
					</div>
					<div class="form-group width50">
						<label for="status">Status: <span class="red"> &#42; </span></label><br>
						<select name="status">
							<option value="">--- Select status ---</option>
							<option value="1" <?php echo ($user['status'] == 1) ? 'selected' : ''; ?> >Hoạt động</option>
							<option value="2" <?php echo ($user['status'] == 2) ? 'selected' : ''; ?> >Khóa</option>
						</select>
					</div>
					
					<input type="hidden" name="upd_id" value='<?php echo $_SESSION["admin_user"]["id"]; ?>'>
					<div class="form-group width100 text-center">
						<button type="submit" name="save" class="btn btn-danger" value="editUser">Save</button>
						<button type="button" class="btn-a btn-primary"><a href="index.php?c=users&a=index">Cancel</a></button>
					</div>
				</form>
			<?php 
			}
			?>
			<div class="clear"></div>
		</div>		
	</div>
</section>			
			
<?php
	require 'views/layouts/bottom.php';
} else {
	header("location:index.php?c=login&a=login");
}
?>
