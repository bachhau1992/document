-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2018 at 07:20 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'super:1, admin:2',
  `ins_id` int(11) NOT NULL COMMENT '登録者ID',
  `upd_id` int(11) DEFAULT NULL COMMENT '更新者ID',
  `ins_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '登録日時',
  `upd_datetime` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時',
  `del_flag` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '削除フラグ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `password`, `email`, `avatar`, `role_type`, `ins_id`, `upd_id`, `ins_datetime`, `upd_datetime`, `del_flag`) VALUES
(1, 'hoangph', '4297f44b13955235245b2497399d7a93', 'hoangph.paraline@gmail.com', '20180514142516.jpg', '1', 1, 1, '2018-05-09 03:06:02', '2018-05-14 07:25:16', '0'),
(2, 'hoang2', '4297f44b13955235245b2497399d7a93', 'hoang2@gmail.com', NULL, '2', 1, 1, '2018-05-11 02:47:14', '2018-05-11 02:47:14', '0'),
(3, 'admin1', '4297f44b13955235245b2497399d7a93', 'admin1@gmail.com', NULL, '2', 1, 1, '2018-05-11 06:42:00', '2018-05-11 06:42:00', '0'),
(4, 'admin4', '4297f44b13955235245b2497399d7a93', 'admin4@gmail.com', '20180514102304.jpg', '2', 1, 1, '2018-05-11 07:42:34', '2018-05-15 10:21:29', '0'),
(5, 'admin2', '4297f44b13955235245b2497399d7a93', 'admin2@gmail.com', '20180514084403.jpg', '2', 1, 6, '2018-05-03 01:44:03', '2018-05-15 16:59:51', '0'),
(6, 'demo', '4297f44b13955235245b2497399d7a93', 'pdemo@gmail.com', '20180515234330.jpg', '1', 1, 1, '2018-05-30 16:43:30', '2018-05-15 16:51:44', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '1:hoạt động, 2:khóa',
  `ins_id` int(11) NOT NULL COMMENT '登録者ID',
  `upd_id` int(11) DEFAULT NULL COMMENT '更新者ID',
  `ins_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '登録日時',
  `upd_datetime` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時',
  `del_flag` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '削除フラグ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `facebook_id`, `status`, `ins_id`, `upd_id`, `ins_datetime`, `upd_datetime`, `del_flag`) VALUES
(1, 'PH Hoang', 'phhoang@gmail.com', '1231312314645121', '1', 1, 6, '2018-05-09 09:54:53', '2018-05-15 17:19:35', '0'),
(2, 'hoatv', 'hoale1195@yahoo.com', '1231231231231123', '1', 1, 3, '2018-05-10 05:39:29', '2018-05-15 16:57:43', '0'),
(5, 'B Hoang', 'bhoang@gmail.com', '1127051477448889', '1', 1, 1, '2018-05-15 03:55:18', '2018-05-15 03:55:18', '0'),
(6, 'E Hoang', 'xhoang@gmail.com', '1127051477448811', '1', 1, 6, '2018-05-15 09:31:53', '2018-05-15 16:55:42', '0'),
(7, 'Y Hoang', 'ahoang@gmail.com', '1127051477448821', '2', 1, 6, '2018-05-15 09:55:03', '2018-05-15 16:56:02', '0'),
(8, 'Hoang Pham', 'hoangpham2395@gmail.com', '1127051477448880', '1', 1, 1, '2018-05-15 10:52:12', '2018-05-15 10:52:12', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
