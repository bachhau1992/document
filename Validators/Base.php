<?php 
class Validators_Base {
	protected $_errors = [];

	public function addError($field, $messages){
			$this->_errors[$field] = $messages;
			return $this;
	}

	public function required($field) {
		return (empty($field)) ? true : false;
	}

	public function checkImage($type) {
		return ($type == 'jpg' || $type == 'jpeg' || $type == 'png' || $type == 'gif') ? true : false;
	}

	public function checkFileSize($file) {
		return ($_FILES[$file]['size'] < 600000) ? true : false;
	}

	public function errors(){
			return $this->_errors;
	}
}
