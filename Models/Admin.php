<?php 
class Models_Admin extends Models_Base 
{
	// Object
	private $id;
	public $name;
	private $password;
	public $email;
	private $avatar;
	public $role_type;
	public $ins_id;
	public $upd_id;
	public $ins_datetime;
	public $upd_datetime;
	public $del_flag;

	protected $_tableName = "admin";

	private function setId($id) {
		$this->id = $id;
	}
	private function getId() {
		return $this->id;
	}

	public function setName($name) {
		$this->name = $name;
	}
	public function getName() {
		return $this->name;
	}

	private function setPassword($password) {
		$this->password = $password;
	}
	private function getPassword() {
		return $this->password;
	}

	public function setEmail($email) {
		$this->email = $email;
	}
	public function getEmail() {
		return $this->email;
	}

	private function setAvatar($avatar) {
		$this->avatar = $avatar;
	}
	private function getAvatar() {
		return $this->avatar;
	}

	public function setRoleType($role_type) {
		$this->role_type = $role_type;
	}
	public function getRoleType() {
		return $this->role_type;
	}

	public function setInsId($ins_id) {
		$this->ins_id = $ins_id;
	}
	public function getInsId() {
		return $this->ins_id;
	}

	public function setUpdId($upd_id) {
		$this->upd_id = $upd_id;
	}
	public function getUpdId() {
		return $this->upd_id;
	}

	public function setInsDatetime($ins_datetime) {
		$this->ins_datetime = $ins_datetime;
	}
	public function getInsDatetime() {
		return $this->ins_datetime;
	}

	public function setUpdDatetime($upd_datetime) {
		$this->upd_datetime = $upd_datetime;
	}
	public function getUpdDatetime() {
		return $this->upd_datetime;
	}

	public function setDelFlag($del_flag) {
		$this->del_flag = $del_flag;
	}
	public function getDelFlag() {
		return $this->del_flag;
	}

	public function checkLogin($email, $password) {
		$sql = "SELECT * FROM ". $this->_tableName ." WHERE email = '". $email."' and password = '". $password."' AND del_flag = 0";
		$result = $this->fetchOne($sql);
		return $result;
		// return $sql;
	}

	public function create($data = []) {
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$sql = "INSERT INTO admin(name, password, email, avatar, role_type, ins_id, upd_id, ins_datetime, upd_datetime, del_flag) VALUES ('".$data['name']."', '".$data['password']."', '".$data['email']."', '".$data['avatar']."', '".$data['role_type']."', '".$data['ins_id']."', '".$data['ins_id']."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '0')";
		// return $sql;
		$result = $this->_connect->execute($sql);
		return ($result) ? true : false;
	}

	public function checkOldPassword($password) {
		$sql = "SELECT * FROM admin WHERE password = '".$password."' AND del_flag = 0";
		$result = $this->fetchOne($sql);
		return ($result) ? true : false;
	}

	public function update($id, $data = []) {
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$sql = "UPDATE admin SET name = '".$data['name']."', email = '".$data['email']."', avatar = '".$data['avatar']."', role_type = '".$data['role_type']."', upd_id = '".$data['upd_id']."', upd_datetime = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."'";
		$result = $this->_connect->execute($sql);
		return ($result) ? true : false;
	}

	public function uniqueEmail ($email) {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE email = '".$email."' AND del_flag = 0";
		$result = $this->fetchOne($sql);
		return ($result) ? false : true;
	}

	public function findSuperAdmin () {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE role_type = 1 AND del_flag = 0";
		$result = $this->fetchOne($sql);
		return $result;
	}
}
