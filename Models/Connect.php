<?php 
/**
 * Connect MySQL 
 */
class Models_Connect extends Config_Connect
{
	public $conn;
	
	public function getConnection() {
		$this->conn = mysqli_connect($this->_db_host, $this->_db_username, $this->_db_password, $this->_db_name);

		if ($this->conn->connect_error) {
			die("Connect failed: ".$this->conn->connect_error);
		}
		return $this->conn;
	}

	public function execute ($sql) {
		$this->getConnection();
		return mysqli_query($this->conn, $sql);
	}

	public function close() {
		return mysqli_close($this->conn);
	}
}
?>