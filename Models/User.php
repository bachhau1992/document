<?php 
class Models_User extends Models_Base{

	protected $_tableName = 'users';

	public $id;
	public $name;
	public $email;
	public $facebook_id;
	public $status;
	public $ins_id;
	public $ins_datetime;
	public $upd_id;
	public $upd_datetime;
	public $del_flag;

	public function create($data = []) {
		$sql = "INSERT INTO ".$this->_tableName." (name, email, facebook_id, status, ins_id, ins_datetime, upd_id, upd_datetime, del_flag) VALUES ('".$data['name']."', '".$data['email']."', '".$data['facebook_id']."', '".$data['status']."', '".$data['ins_id']."', '".$data['ins_datetime']."', '".$data['ins_id']."', '".$data['ins_datetime']."', 0)";
		$result = $this->_connect->execute($sql);

		return ($result) ? true : false;
	}

	public function update($id, $data = []) {
		date_default_timezone_get("Asia/Ho_Chi_Minh");

		$sql = "UPDATE ".$this->_tableName." SET name = '".$data['name']."', email = '".$data['email']."', facebook_id = '".$data['facebook_id']."', status = '".$data['status']."', upd_id = '".$data['upd_id']."', upd_datetime = '".date('Y-m-d H:i:s')."' WHERE id = ".$id;

		$result = $this->_connect->execute($sql);
		return ($result) ? true : false;
	}

	public function checkAccountFB ($facebook_id) {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE facebook_id = '$facebook_id' AND del_flag = 0";
		$result = $this->_connect->execute($sql);
		return ($result->num_rows == 0) ? false : true;
	}

	public function checkStatus ($facebook_id) {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE facebook_id = '$facebook_id' AND del_flag = 0 AND status = 1";
		$result = $this->_connect->execute($sql);
		return ($result->num_rows == 0) ? false : true;
	}
}
